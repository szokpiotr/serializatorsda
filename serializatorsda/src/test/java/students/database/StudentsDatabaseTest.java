package students.database;

import org.junit.Test;

public class StudentsDatabaseTest {
	
	@Test
	public void addStudentTest() {
		StudentDatabase database = new StudentDatabase();
		database.addStudent("Piotr", "Kowalski");
		
		assert 1 == 1;
	}
	
	public void getStudentTest() {
		StudentDatabase database = new StudentDatabase();
		database.addStudent("Piotr", "Kowalski");
		
		assert database.getStudent(13000).getName().equals("Piotr");
	}
	
	public void removeTest() {
		StudentDatabase database = new StudentDatabase();
		database.addStudent("Piotr", "Kowalski");
		database.remove(13000);
		
		assert database.getAll().size() == 1;
	}
	
}
