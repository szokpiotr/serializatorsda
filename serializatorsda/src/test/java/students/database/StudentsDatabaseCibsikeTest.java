package students.database;

import java.io.FileNotFoundException;
import java.io.IOException;

public class StudentsDatabaseCibsikeTest {
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		DatabaseSerializator databaseSerializator = new DatabaseSerializator();
		
		StudentDatabase database = new StudentDatabase();
		database.addStudent("Piotr", "Szok");
		database.addStudent("Kasia", "Szok");
		
		databaseSerializator.save(database);
		
		StudentDatabase loaded = databaseSerializator.load();
		
		for(Student student : loaded.getAll()){
			System.out.println(student.getName());
			System.out.println(student.getSurname());
			System.out.println(student.getIndexNumber());
		}
		
	}

}
