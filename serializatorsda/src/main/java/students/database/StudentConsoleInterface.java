package students.database;

import java.util.Scanner;

public class StudentConsoleInterface {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		StudentDatabase baza = new StudentDatabase();
		
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] splits = line.split(" ");
 
			if(line.equals("quit")){
				break;
			}else if(splits[0].equals("add")){
				String imie = splits[1];
				String nazwisko = splits[2];
				baza.addStudent(imie, nazwisko);
				continue;
			}else if(splits[0].equals("print")){
//				System.out.println("Baza student�w");
				long cos = Long.parseLong(splits[1]);
				baza.getStudent(cos);
				continue;
			}
			else if(splits[0].equals("remove")){
				long cos = Long.parseLong(splits[1]);
				baza.remove(cos);
				continue;
			}
			

		}
	}

}
