package students.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentDatabase implements Serializable {
	Map<Long, Student> mapa = new HashMap<>();
	static long indeks = 13000;

	public void addStudent(String imie, String nazwisko) {
		mapa.put(indeks, new Student(indeks, imie, nazwisko));
		System.out.println("Student " + imie + " " + nazwisko + " ma indeks: " + indeks);
		indeks++;
	}

	public Student getStudent(long numerIndeksu) throws NoSuchStudentException {
		if(!mapa.containsKey(numerIndeksu))
		{
			throw new NoSuchStudentException();
		}
		return mapa.get(numerIndeksu);
	}
	
	public List<Student> getAll(){
        return new ArrayList<Student>(mapa.values());
    }

	public void remove(long numerIndeksu) {
		System.out.println("Student "+numerIndeksu+" "+mapa.get(numerIndeksu).getName()+" "+mapa.get(numerIndeksu).getSurname()+" zostal usuniety");
		mapa.remove(numerIndeksu);
	}

}
