package students.database;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationOfStudents {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("student.txt"));
		Student doZapisu = new Student(100, "Jan", "Kowlaki");
		stream.writeObject(doZapisu);
		stream.close();
	}
}
