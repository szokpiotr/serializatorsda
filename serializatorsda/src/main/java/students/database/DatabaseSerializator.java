package students.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DatabaseSerializator {
	
	public void save(StudentDatabase database) throws FileNotFoundException, IOException{
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("studenty.txt"));
		stream.writeObject(database);
		stream.close();
	}
	
	public StudentDatabase load() throws FileNotFoundException, IOException, ClassNotFoundException{
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("studenty.txt"));
		StudentDatabase base = (StudentDatabase) inputStream.readObject();
		inputStream.close();
		return base;
	}

}
