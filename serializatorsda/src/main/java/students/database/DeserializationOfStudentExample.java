package students.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationOfStudentExample {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub

		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("student.txt"));
		Student s = (Student) inputStream.readObject();

		System.out.println(s.getIndexNumber());
		System.out.println(s.getName());
		System.out.println(s.getSurname());
	}
}
