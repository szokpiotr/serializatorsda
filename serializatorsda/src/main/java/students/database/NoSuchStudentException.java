package students.database;

public class NoSuchStudentException extends RuntimeException {

	public NoSuchStudentException() {
		super("Nie ma studenta");
	}
}
