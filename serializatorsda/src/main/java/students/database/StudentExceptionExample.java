package students.database;

public class StudentExceptionExample {
	public static void main(String[] args) {
		StudentDatabase base = new StudentDatabase();
		base.addStudent("Piotr", "Kowalski");
		
		System.out.println(base.getStudent(13000).getName());
		System.out.println(base.getStudent(13005).getName());
	}
}
