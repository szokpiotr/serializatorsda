package zbiory;

import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import java.util.TreeSet;

public class TabSet {

	public static void main(String[] args) {
		Integer[] tab = {1,10000,2,6,7,1,3,4,2,2,3,2,4,5};
		
		Set<Integer> mySet = new TreeSet<Integer>();
		
		mySet.addAll(Arrays.asList(tab));
		
		System.out.println("Elementy zbioru to "+mySet);
		System.out.println("Rozmiar zbioru to "+mySet.size());
		
		for(Integer element : mySet){
			System.out.println("Kolejny element zbioru to "+element);
		}
		
		mySet.remove(1);
		mySet.remove(2);
		
		System.out.println("Elementy zbioru po usunieciu to "+mySet);
		System.out.println("Rozmiar zbioru po usunieciu to "+mySet.size());

	}
	
}
