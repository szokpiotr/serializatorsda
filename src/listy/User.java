package listy;
import java.util.*;

public class User {
	
	private String name, password;
	
	public User(String name, String password){
		this.name = name;
		this.password = password;
	}
	
	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "Nazwa to "+name+" a haslo to "+password;
	}

	public static void main(String[] args) {
		User user1 = new User("Piotr", "haslo");
		User user2 = new User("Marek", "12345");
		User user3 = new User("Piotr", "haslo");
		
		List<User> lista = new ArrayList<User>();
		
		lista.add(user1);
		lista.add(user2);
		lista.add(user3);

		for(int i=0; i<lista.size(); i++){
			System.out.println(lista.get(i));
		}

	}

}
