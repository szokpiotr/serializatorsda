package listy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ListyZad {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		List<Double> lista = new ArrayList<Double>();
		List<Double> listaLink = new LinkedList<Double>();
		
		double suma = 0;
		for(int i=0; i<5; i++){
			System.out.println("Podaj element listy "+i);
			listaLink.add(scanner.nextDouble());
			suma+=listaLink.get(i);
		}
		
		System.out.println("Suma to "+suma);
		
//		System.out.println("Podaj elementy listy");
//		lista.add(scanner.nextDouble());
//		lista.add(scanner.nextDouble());
//		lista.add(scanner.nextDouble());
//		
//		double iloczyn = 1;
//		for(double element : lista){
//			iloczyn*=element;
//		}
//		
//		System.out.println("Iloczyn to "+iloczyn);
		
		double srednia = suma/listaLink.size();
		System.out.println("Srednia to "+srednia);

	}

}
