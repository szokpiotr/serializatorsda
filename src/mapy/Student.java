package mapy;

public class Student {
	
	private String name, surname;
	private int index;
	private long indeks;
	
	public long getIndeks() {
		return indeks;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public int getIndex() {
		return index;
	}

	public Student(String name, String surname, int index) {
		this.name = name;
		this.surname = surname;
		this.index = index;
	}
	
	public Student(String name, String surname, long indeks) {
		this.name = name;
		this.surname = surname;
		this.indeks = indeks;
	}
	
	@Override
	public String toString() {
		return name+" "+surname;
	}

}
