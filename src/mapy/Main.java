package mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) {
		Student piotr = new Student("Piotr","Szok",132914);
		Student ewelina = new Student("Ewelina","Kawiak",123456);
		Student kasia = new Student("Kasia","Szok",100400);

        Map<Integer, Student> myMap = new HashMap<Integer, Student>();
		
        myMap.put(piotr.getIndex(), piotr);
        myMap.put(ewelina.getIndex(), ewelina);
        myMap.put(kasia.getIndex(), kasia);
        
        System.out.println(myMap.containsKey(132914));
        
        System.out.println(myMap.get(100400));
        System.out.println("Size of the map: "+ myMap.size());
        System.out.println("Mapa to "+myMap);

	}

}
