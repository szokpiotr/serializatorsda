package mapy;

import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;

public class Uczelnia {
	
	Map<Long, Student> mapa= new HashMap<>();
	
	public void dodajStudenta(String imie, String nazwisko, long numerIndeksu){
		mapa.put(numerIndeksu,new Student(imie,nazwisko,numerIndeksu));
	}

	public boolean czyIstniejeStudent(long numerIndeksu){
		if(!mapa.containsKey(numerIndeksu)){
			return false;
		}
		return true;
	}
	
	public Student pobierzStudenta(long numerIndeksu){
		return mapa.get(numerIndeksu);
	}
	
	public int liczbaStudentow(){
		return mapa.size();
	}
	
	public void wypiszWszystkich(){
		System.out.println(mapa);
	}
	
}
