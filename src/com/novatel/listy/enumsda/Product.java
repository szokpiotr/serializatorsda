package com.novatel.listy.enumsda;

public class Product {
	private String name;
	private int cena;
	private Category category;
	
	public String getName() {
		return name;
	}

	public int getCena() {
		return cena;
	}

	public Category getCategory() {
		return category;
	}

	public Product(String name, int cena, Category category){
		this.name=name;
		this.cena=cena;
		this.category = category;
		
	}
	
	@Override
	public String toString() {
		return name+" "+cena;
	}

}
