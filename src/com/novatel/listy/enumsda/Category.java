package com.novatel.listy.enumsda;

public enum Category {
	FOOD, 
	CLOTHES, 
	ELECTRONICS, 
	ALCOHOL

}
