package com.novatel.listy.enumsda;

import java.util.LinkedList;
import java.util.List;
import java.util.*;

public class Main {

	public static void main(String[] args) {

		Product telefon = new Product("telefon", 500, Category.ELECTRONICS);
		Product piwo = new Product("piwo", 5, Category.ALCOHOL);
		Product chleb = new Product("chleb", 2, Category.FOOD);
		
		Store store = new Store();
		
		store.addProduct(telefon);
		store.addProduct(piwo);
		store.addProduct(chleb);
		
		System.out.println("Mozliwe kategorie to FOOD, CLOTHES, ELECTRONICS, ALCOHOL");
		Scanner scanner = new Scanner(System.in);
		
		String kategoria = scanner.next();
		
		
		store.showProduct(Category.valueOf(kategoria));

	}

}
