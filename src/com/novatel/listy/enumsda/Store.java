package com.novatel.listy.enumsda;
import java.util.*;

public class Store {
	
	private List<Product> lista = new LinkedList<Product>();
	
	public void addProduct(Product product){
		lista.add(product);
	}
	
	public void showProduct(Category category){
		for(Product product : lista){
			if(category == product.getCategory()){
			System.out.println(product);
			}
		}
	}

}
