package com.sda.zus;

public enum ClientConcern {
	REGISTER,
	CHECK_REGISTRATION
}
