package com.sda.zus;

public class Main {

	public static void main(String[] args) {
		WaitingRoom poczekalnia = new WaitingRoom();
		
		poczekalnia.addClient(new Client("Piotr","Szok","91030403552", ClientConcern.REGISTER));
		poczekalnia.addClient(new Client("Ewelina","Szok","910603552", ClientConcern.REGISTER));
		poczekalnia.addClient(new Client("Katarzyna","Szok","123456789", ClientConcern.CHECK_REGISTRATION));
		
		OfficeEmployee urzednik = new OfficeEmployee("Adam", "Konieczko");
		
//		urzednik.registerClient(poczekalnia.handleClient());
//		urzednik.registerClient(poczekalnia.handleClient());
//		urzednik.registerClient(poczekalnia.handleClient());
//		urzednik.registerClient(poczekalnia.handleClient());
		
		urzednik.handleClient(poczekalnia.handleClient());
		urzednik.handleClient(poczekalnia.handleClient());
		urzednik.handleClient(poczekalnia.handleClient());
		urzednik.handleClient(poczekalnia.handleClient());

	}

}
