package com.sda.zus;

import java.util.LinkedList;
import java.util.List;

public class WaitingRoom {
	LinkedList<Client> clients = new LinkedList<Client>();
	private static int number = 0;
	
	public void addClient(Client client){
		clients.add(client);
		System.out.println("W kolejce jest "+clients.size()+" os�b, a jej numer to "+(++number));
	}
	
	public Client handleClient(){
		if(!clients.isEmpty()){
			Client current = clients.getFirst();
			System.out.println("Obsluguje klienta "+current.getName()+current.getSurname());
			clients.removeFirst();
			return current;
		}
		return null;
	}
}


