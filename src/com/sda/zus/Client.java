package com.sda.zus;

public class Client {
	
	private String name, surname, pesel;
	private ClientConcern concern;
	
	public ClientConcern getConcern() {
		return concern;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getPesel() {
		return pesel;
	}

	public Client(String name, String surname, String pesel, ClientConcern concern){
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
		this.concern = concern;
	}
	
	@Override
	public String toString() {
		return name+" "+surname;
	}

}
