package com.sda.zus;

public class OfficeEmployee {
	private String name, surname;

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public OfficeEmployee(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}

	public void registerClient(Client clientToRegistry) {
		if (clientToRegistry != null) {
			ZUSRegistry.checkIfRegistered(clientToRegistry.getPesel());
			ZUSRegistry.register(clientToRegistry);
		} else {
			System.out.println("Kolejka jest pusta!");
		}

	}

	public void handleClient(Client client) {
		if (client != null) {
			if (client.getConcern() == ClientConcern.REGISTER) {
				this.registerClient(client);
			}
			if (client.getConcern() == ClientConcern.CHECK_REGISTRATION) {
				this.checkClientStatus(client);
			}
		}
	}

	public void checkClientStatus(Client client) {
		if (client != null) {
			System.out.println("Status klienta to "+ZUSRegistry.checkIfRegistered(client.getPesel())+" bo wywolalem metode checkClientStatus");
		} else {
			System.out.println("Kolejka jest pusta!!");
		}
	}

}
