package com.sda.zus;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ZUSRegistry {
	
	private static HashMap<String, Client> registry = new HashMap<String, Client>();
	
	public static HashMap<String, Client> getRegistry() {
		System.out.print("Ca�y rejestr klient�w to: ");
		return registry;
	}

	public static void register(Client clientToRegistry){
		System.out.println("Klient "+clientToRegistry.getName()+" "+clientToRegistry.getSurname()+" zosta� zarejestrowany");
		registry.put(clientToRegistry.getPesel(), clientToRegistry);
	}
	
	public static boolean checkIfRegistered(String pesel){
		System.out.println("Wartosc w rejestrze to "+registry.containsKey(pesel));
		return registry.containsKey(pesel);
	}
	
	

}
