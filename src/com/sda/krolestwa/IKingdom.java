package com.sda.krolestwa;

import java.util.*;

public interface IKingdom {
	List<AbstractCharacter> getArmy();
	void addWarrior();
	void addArcher();
	void addMage();
}
