package com.sda.krolestwa;

public class OrcWarrior extends AbstractCharacter implements IWarrior {

	@Override
	public void attack(AbstractCharacter character) {
		character.decreaseHealth(15);
		this.decreaseEnergy(7);
	}

}
