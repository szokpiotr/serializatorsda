package com.sda.krolestwa;

public class OrcMage extends AbstractCharacter implements IWarrior {

	private int arrows;

	@Override
	public void attack(AbstractCharacter character) {
			character.decreaseHealth(7);
			this.decreaseEnergy(11);
			character.decreaseEnergy(6);
	}

}
