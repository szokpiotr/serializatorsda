package com.sda.krolestwa;

public class Main {

	public static void main(String[] args) {
		OrcKingdom temeria = new OrcKingdom();
		OrcKingdom wezania = new OrcKingdom();

		for(int i=0; i<5; i++){
			temeria.addWarrior();
		}
		
		for(int i=0; i<2; i++){
			temeria.addArcher();
		}
		
		for(int i=0; i<5; i++){
			wezania.addMage();
		}
		
		for(int i=0; i<2; i++){
			wezania.addArcher();
		}
		
		System.out.println(temeria.getArmy().get(0));
		System.out.println(wezania.getArmy().get(0));
		
		temeria.getArmy().get(0).attack(wezania.getArmy().get(0));
		
		System.out.println(temeria.getArmy().get(0));
		System.out.println(wezania.getArmy().get(0));
	}

}
