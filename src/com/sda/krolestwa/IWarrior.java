package com.sda.krolestwa;

public interface IWarrior {
	void attack(AbstractCharacter character);
}
