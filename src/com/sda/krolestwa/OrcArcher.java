package com.sda.krolestwa;

public class OrcArcher extends AbstractCharacter implements IWarrior {

	private int arrows;
	
	public OrcArcher() {
		super();
		this.arrows = 10;
	}
	
	public OrcArcher(int life, int energy, int arrows) {
		super(life, energy);
		this.arrows = arrows;
	}

	public int decreaseArrows() {
		return arrows - 1;
	}

	@Override
	public void attack(AbstractCharacter character) {
		if (arrows > 0) {
			character.decreaseHealth(10);
			this.decreaseEnergy(8);
			this.decreaseArrows();
		}
		else{
			System.out.println("Nie mam strza�!");
		}
	}

}
