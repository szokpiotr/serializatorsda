package com.sda.krolestwa;

public class AbstractCharacter implements IWarrior{
	
	private int life, energy;

	public AbstractCharacter() {
		this.life = 100;
		this.energy = 100;
	}

	public AbstractCharacter(int life, int energy) {
		super();
		this.life = life;
		this.energy = energy;
	}
	
	public void decreaseHealth(int points){
		this.life = this.life - points;
	}
	
	public void decreaseEnergy(int points){
		this.energy = this.energy - points;
	}
	
	public int getHealth(){
		return life;
	}
	
	public int getEnergy(){
		return energy;
	}
	
	public void attack(AbstractCharacter character){
		
	}
	
	@Override
	public String toString() {
		return "Wojonik ma zycia "+life+" i energii "+energy;
	}

}
