package com.sda.krolestwa;

import java.util.*;

public class OrcKingdom implements IKingdom {
	
	private List<AbstractCharacter> orcs = new LinkedList<AbstractCharacter>();

	@Override
	public List<AbstractCharacter> getArmy() {
		return orcs;
	}

	@Override
	public void addWarrior() {
		AbstractCharacter warrior = new OrcWarrior();
		orcs.add(warrior);
	}

	@Override
	public void addArcher() {
		AbstractCharacter archer = new OrcArcher();
		orcs.add(archer);
	}

	@Override
	public void addMage() {
		AbstractCharacter mage = new OrcMage();
		orcs.add(mage);
	}


}
